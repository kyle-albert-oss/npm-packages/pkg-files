import * as fs from "fs-extra";
import * as path from "path";

export interface IFileInfo {
	absPath: string;
	entry: fs.Dirent;
	ext: string | undefined;
	relPath: string;
}

export const getImmediateChildFiles = (dir: string): IFileInfo[] =>
	fs
		.readdirSync(dir, { withFileTypes: true })
		.map(
			(entry): IFileInfo => ({
				entry,
				absPath: path.resolve(path.join(dir, entry.name)),
				ext: entry.isDirectory() ? undefined : path.extname(entry.name).substring(1),
				relPath: entry.name,
			})
		)
		.sort((fi1, fi2) => {
			if ((fi1.entry.isDirectory() && fi2.entry.isDirectory()) || (!fi1.entry.isDirectory() && !fi2.entry.isDirectory())) {
				return fi1.entry.name.localeCompare(fi2.entry.name);
			} else if (fi1.entry.isDirectory()) {
				return 1;
			}
			return -1;
		});

export interface IGetAllChildFilesRecursivelyOpts {
	filter?(fi: IFileInfo): any;
	excludeDirectoryEntries?: boolean;
}

const allChildFilesRecurse = (startDir: string, dir: string, opts?: IGetAllChildFilesRecursivelyOpts) => {
	const files: IFileInfo[] = [];

	const childFiles = getImmediateChildFiles(dir).map((fi) => {
		fi.relPath = path.relative(startDir, fi.absPath);
		return fi;
	});

	for (const childFile of childFiles) {
		if (opts?.filter && !opts.filter(childFile)) {
			continue;
		}
		const isDirectory = childFile.entry.isDirectory();
		if (!isDirectory || !opts?.excludeDirectoryEntries) {
			files.push(childFile);
		}
		if (isDirectory) {
			files.push(...allChildFilesRecurse(startDir, childFile.absPath));
		}
	}

	return files;
};

/**
 * Recursively gets all child files for the specified directory. Optionally takes a filter function that can allow prevention of traversing
 * entire subdirectories or excluding files with certain characteristics.
 * @param dir
 * @param opts
 */
export const getAllChildFilesRecursively = (dir: string, opts?: IGetAllChildFilesRecursivelyOpts): IFileInfo[] =>
	allChildFilesRecurse(dir, dir, opts);
