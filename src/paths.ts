import * as fs from "fs-extra";
import * as path from "path";

export const getFilenameNoExt = (pathOrFileName: string): string => path.basename(pathOrFileName, path.extname(pathOrFileName));

export interface IResolveExistingResult {
	absolute: string;
	from: string;
	relative: string;
}

/**
 * Resolves an existing path {to} relative to the given {from} paths.
 *
 * Given a single {to} path:
 *  If {to} is existing absolute path it is returned.
 *  If {to} resolves against multiple {from} paths, the last one according to the order of {from} is chosen.
 *
 * If there are multiple {to} paths:
 *  The algorithm for a single {to} path is repeated in order of {to} until the first existing path is resolved.
 *
 * @param to - path to resolve or an array of paths to support variations
 * @param from - if to is a relative path, paths to resolve from
 * @param unshiftCwd - true to add process.cwd() to the beginning of the {from list.
 */
export const resolveExistingFromPaths = (
	to: string | readonly string[],
	from: readonly string[] | undefined = undefined,
	unshiftCwd: boolean = true
): IResolveExistingResult | null => {
	if (typeof to === "string" && path.isAbsolute(to)) {
		return fs.existsSync(to) ? { absolute: to, from: process.cwd(), relative: path.relative(process.cwd(), to) } : null;
	}
	const cwd = process.cwd();
	if (unshiftCwd) {
		from = [cwd, ...(from ?? [])];
	}

	const tos = typeof to === "string" ? [to] : to;
	for (const currentTo of tos) {
		const paths = from
			?.map(
				(f): IResolveExistingResult => {
					const absolute = path.resolve(f, currentTo);
					return {
						absolute,
						from: f,
						relative: path.relative(f, absolute),
					};
				}
			)
			.filter(({ absolute }) => fs.existsSync(absolute));

		const lastResolved = paths?.[paths.length - 1];
		if (lastResolved) {
			return lastResolved;
		}
	}

	return null;
};
