import * as path from "path";
import { getAllChildFilesRecursively } from "../scan";

describe("getAllChildFilesRecursively", () => {
	it("generally works.", () => {
		const childFiles = getAllChildFilesRecursively(path.resolve("src"));

		const firstLevelFile = childFiles.find((fi) => fi.relPath === "index.ts");
		expect(firstLevelFile).toBeDefined();
		expect(firstLevelFile?.entry.isDirectory()).toEqual(false);

		const firstLevelDir = childFiles.find((fi) => fi.relPath === "__tests__");
		expect(firstLevelDir).toBeDefined();
		expect(firstLevelDir?.entry.isDirectory()).toEqual(true);

		const secondLevelFile = childFiles.find((fi) => fi.relPath === "__tests__/scan.test.ts");
		expect(secondLevelFile).toBeDefined();
		expect(secondLevelFile?.ext).toEqual("ts");
	});

	it("excludes directory entries from result when specified.", () => {
		const childFiles = getAllChildFilesRecursively(path.resolve("src"), { excludeDirectoryEntries: true });

		const firstLevelFile = childFiles.find((fi) => fi.relPath === "index.ts");
		expect(firstLevelFile).toBeDefined();

		const firstLevelDir = childFiles.find((fi) => fi.relPath === "__tests__");
		expect(firstLevelDir).toBeUndefined();

		const secondLevelFile = childFiles.find((fi) => fi.relPath === "__tests__/scan.test.ts");
		expect(secondLevelFile).toBeDefined();
	});

	it("filters entire subdirectories correctly.", () => {
		const childFiles = getAllChildFilesRecursively(path.resolve("src"), {
			filter: (fi) => !fi.entry.isDirectory() || fi.entry.name !== "__tests__",
		});

		const firstLevelFile = childFiles.find((fi) => fi.relPath === "index.ts");
		expect(firstLevelFile).toBeDefined();

		const firstLevelDir = childFiles.find((fi) => fi.relPath === "__tests__");
		expect(firstLevelDir).toBeUndefined();

		const secondLevelFile = childFiles.find((fi) => fi.relPath === "__tests__/scan.test.ts");
		expect(secondLevelFile).toBeUndefined();
	});
});
