import { resolveExistingFromPaths, getFilenameNoExt } from "../paths";
import * as path from "path";

const cwd = process.cwd();
const data1AbsPath = path.join(__dirname, "data1");
const data1MultipleTxtAbsPath = path.join(data1AbsPath, "multiple.txt");
const data2AbsPath = path.join(__dirname, "data2");
const data1CwdRelPath = path.relative(cwd, data1AbsPath);
const data2CwdRelPath = path.relative(cwd, data2AbsPath);

describe("getFilenameNoExt", () => {
	it("works with absolute path.", () => {
		expect(getFilenameNoExt(data1MultipleTxtAbsPath)).toEqual("multiple");
	});

	it("works with a filename with extension.", () => {
		expect(getFilenameNoExt("something.or")).toEqual("something");
		expect(getFilenameNoExt("something.or.other")).toEqual("something.or");
	});

	it("works with no extension.", () => {
		expect(getFilenameNoExt("something")).toEqual("something");
	});
});

describe("resolveExistingFromPaths", () => {
	describe("to: absolute", () => {
		it("returns the same path as absolute resolution with relative resolution from the cwd.", () => {
			const resolved = resolveExistingFromPaths(path.join(data1AbsPath, "multiple.txt"), [data1AbsPath, data2AbsPath]);

			expect(resolved?.absolute).toEqual(path.join(data1AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(cwd);
			expect(resolved?.relative).toEqual(path.join(data1CwdRelPath, "multiple.txt"));
		});

		it("with no froms, returns the same path as absolute resolution with relative resolution from the cwd.", () => {
			const resolved = resolveExistingFromPaths(path.join(data1AbsPath, "multiple.txt"));

			expect(resolved?.absolute).toEqual(path.join(data1AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(cwd);
			expect(resolved?.relative).toEqual(path.join(data1CwdRelPath, "multiple.txt"));
		});

		it("returns null if to doesn't exist.", () => {
			const resolved = resolveExistingFromPaths(path.join(data1AbsPath, "multiple-123.txt"), [data1AbsPath, data2AbsPath]);

			expect(resolved).toBeNull();
		});
	});

	describe("to: relative", () => {
		it("returns the last path according to froms ordering if to exists in more than 1 directory.", () => {
			const resolved = resolveExistingFromPaths("multiple.txt", [data1AbsPath, data2AbsPath]);

			expect(resolved?.absolute).toEqual(path.join(data2AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(data2AbsPath);
			expect(resolved?.relative).toEqual("multiple.txt");
		});

		it("resolves from cwd by default.", () => {
			const resolved = resolveExistingFromPaths(path.join(data1CwdRelPath, "multiple.txt"));

			expect(resolved?.absolute).toEqual(path.join(data1AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(cwd);
			expect(resolved?.relative).toEqual(path.join(data1CwdRelPath, "multiple.txt"));
		});

		it("resolves to narrowed relative path.", () => {
			const resolved = resolveExistingFromPaths(path.join(data1CwdRelPath, "multiple.txt"), [data1AbsPath, data2AbsPath]);

			expect(resolved?.absolute).toEqual(path.join(data1AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(cwd);
			expect(resolved?.relative).toEqual(path.join(data1CwdRelPath, "multiple.txt"));
		});
	});

	describe("multiple tos", () => {
		it("resolves the first existing to.", () => {
			const resolved = resolveExistingFromPaths(["non-exist.txt", "multiple", "multiple.txt"], [data1AbsPath, data2AbsPath]);

			expect(resolved?.absolute).toEqual(path.join(data2AbsPath, "multiple.txt"));
			expect(resolved?.from).toEqual(data2AbsPath);
			expect(resolved?.relative).toEqual("multiple.txt");
		});
	});
});
