/* eslint-env commonjs */
/**
 * https://jestjs.io/docs/en/configuration
 */
module.exports = {
	testEnvironment: "node",
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/"],
	//	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
};
